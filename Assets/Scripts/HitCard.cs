﻿using System;
using System.Collections.Generic;
using System.Linq;
using TienLen;

public class HitCard
{
	private int _type = -1;
    private int _maxCard = -1;
    private int _playerHit = -1;
    private int _roundIndex = -1;
    private ulong _listCard = 0;
    public HitCard(int t, int p, int indexRound, List<int> list)
    {
        _type = t;
        _playerHit = p;
        _roundIndex = indexRound;
    }

 //   public HitCard (int t, int p, List<int> list)
	//{
	//	_type = t;		
	//	_playerHit = p;
 //       if (list == null) return;
 //   }

    public HitCard(int indexP, int _roundIndex,  List<int> list)
    {
        _playerHit = indexP;
        _type = GameRules.KiemTraBaiHopLe(list);
        _listCard = BitsHandle.ListToUlong(list);
        foreach (var c in list)
            if (c > _maxCard) _maxCard = c;
    }

    public HitCard(int indexP, int _roundIndex, ulong listLong)
    {
        _playerHit = indexP;
        var list = BitsHandle.UlongToList(listLong);
        _type = GameRules.KiemTraBaiHopLe(list);
        _listCard = BitsHandle.ListToUlong(list);
        foreach (var c in list)
            if (c > _maxCard) _maxCard = c;
    }

    //public HitCard (HitCard hit, int p)
    //{
    //	_playerHit = p;
    //	_type = hit.Type;
    //	_MaxCard = hit.MaxCard;
    //       if (hit._listCard != null)
    //	    _listCard = hit.ListCard;
    //       _roundIndex = hit.RoundIndex;
    //       _tempListCard = new List<int>(_listCard);
    //   }

    public int Type { get { return _type; } }
    public int MaxCard { get { return _maxCard; } }
    public int PlayerHit { get { return _playerHit; } }
    public int RoundIndex { get { return _roundIndex; } }
    /// <summary>
    /// List nay` can` duoc chong' hacker
    /// </summary>
    public List<int> ListCard { get {
            return BitsHandle.UlongToList(_listCard);
        } }
    public ulong ListCard_Long
    {
        get
        {
            return _listCard;
        }
    }
    //public void SetType(int t)
    //{
    //    _type = t;
    //}

    //public void SetMaxCard(int m)
    //{
    //    _MaxCard = m;
    //}

    public void SetPlayerHit(int p)
    {
        _playerHit = p;
    }

    public void SetRoundIndex(int i)
    {
        _roundIndex = i;
    }

    //public void SetListCard(List<int> list)
    //{
    //    for (int i = 0; i < list.Count; i++)
    //    {
    //        _listCard.Add(list[i]);
    //        if (_MaxCard < list[i])
    //            _MaxCard = list[i];
    //    }
    //}
}

public class ThongSoVanBai
{
    private int _soNguoiChoi = 4;
    private long _soTienCuoc = 100;
    private int _level = 0;
    private KieuChoi _kieuChoi = KieuChoi.TruyenThong;
    public ThongSoVanBai()
    {
    }

    public ThongSoVanBai(int sn, long st, int lv, KieuChoi kc)
    {
        _soNguoiChoi = sn;
        _soTienCuoc = st;
        _level = lv;
        _kieuChoi = kc;
    }

    public int SoNguoiChoi { get { return _soNguoiChoi; } }
    public long SoTienCuoc { get { return _soTienCuoc; } }
    public int Level { get { return _level; } }
    public KieuChoi KieuChoi { get { return _kieuChoi; } }
}

public class ThongTinNguoiChoi
{
    private long _soXu = 0;
    private string _ten = "";
    private int _idAvt = 0;

    public ThongTinNguoiChoi(long xu, string ten, int id)
    {
        _soXu = xu;
        _ten = ten;
        _idAvt = id;
    }


    public long SoXu { get { return _soXu; } }
    public string Ten
    {
        get { return _ten; }
        set
        {
            _ten = value;
            if (ChangeHandler != null)
            {
                ChangeHandler(this, EventArgs.Empty);
            }
        }
    }
    public int IdAvt
    {
        get { return _idAvt; }
        set
        {
            _idAvt = value;
            if (ChangeHandler != null)
            {
                ChangeHandler(this, EventArgs.Empty);
            }
        }
    }

    public void ThemXu(long value)
    {
        _soXu += value;

        if (ChangeHandler != null)
        {
            ChangeHandler(this, EventArgs.Empty);
        }
    }

    public ThayDoiEventHandler ChangeHandler;
    public delegate void ThayDoiEventHandler(object sender, EventArgs e);
}