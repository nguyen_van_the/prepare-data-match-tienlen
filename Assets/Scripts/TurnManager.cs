﻿using System.Collections.Generic;
using UnityEngine;

namespace TienLen
{
	public class TurnManager
	{
		public static TurnManager Instance = new TurnManager();
		private int _soNguoiChoi;
		private int _nguoiChoiDauTien;

		private bool[] _vaoVong;
		private bool[] _hadWin;
		//private int _soNguoiChoiInRound = 4;
		private int _lastWinPlayer = -2;
		private bool _needReset = true;
		private bool _huongSai = false;
		private int _currentTurn = -1;
        private Dictionary<int, int> _roundCount = new Dictionary<int, int>(); //chua index player danh lan luot, clear moi khi reset round moi
		public TurnManager (){
			_soNguoiChoi = 4;
			_nguoiChoiDauTien = 0;
			_currentTurn = _nguoiChoiDauTien;
		}

		public TurnManager (int soNguoiChoi, int nguoiChoiDauTien)
		{
			_soNguoiChoi = soNguoiChoi;
			_nguoiChoiDauTien = nguoiChoiDauTien;
			_currentTurn = nguoiChoiDauTien;
            _vaoVong = new bool[_soNguoiChoi];
            _hadWin = new bool[_soNguoiChoi];
            Init();
        }

        public void Init()
        {
            //Debug.Log ("TurnManager: Reset Turn");
            //_soNguoiChoiInRound = _soNguoiChoi;
            _needReset = false;
            for (int i = 0; i < _soNguoiChoi; i++)
            {
                _vaoVong[i] = true;
                _hadWin[i] = false;
                _roundCount[i] = 0;
            }
        }

        public void ResetNewRound()
        {
            //turn off reset switch
            _needReset = false;
            //set player win in round
            int playerWinRound = -1;
            //		Debug.Log ("[TurnManager] Huong Sai: " + huongSai + ", oldCOunt: " + countInRound);
            for (var i = 0; i < _soNguoiChoi; i++)
                if (_vaoVong[i] && !_hadWin[i])
                {
                    playerWinRound = i;
                    break;
                }
            if (_huongSai || playerWinRound == -1)
            {
                //			int indexHuongSai = sequencePlayer [0];
                //int i = 0;
                //for (i = _lastWinPlayer; i < _lastWinPlayer + _soNguoiChoi; i++)
                //{
                //    if (_hadWin[i % _soNguoiChoi] && _vaoVong[i % _soNguoiChoi])
                //        break;
                //}
                //			Debug.Log ("[TurnManager] Reset new round huong sai - i: " + i);
                for (var i = _lastWinPlayer  + 1; i < _lastWinPlayer + 2 * _soNguoiChoi; i++)
                {
                    if (!_hadWin[i % _soNguoiChoi])
                    {
                        playerWinRound = i % _soNguoiChoi;
                        break;
                    }
                }
                Debug.Log("Huong sai!");
            }
            else {                
                Debug.Log("Con duy nhat 1 nguoi!");
            }
            Debug.Log("[TurnManager] Reset new round playerWinRound: " + playerWinRound);
            //int count = 0;
            _currentTurn = playerWinRound;
            for (int i = 0; i < _soNguoiChoi; i++)
            {
                if (!_hadWin[i])
                {
                    _vaoVong[i] = true;
                }
                else
                    _vaoVong[i] = false;
                _roundCount[i] = 0;
            }
            //_soNguoiChoiInRound = _soNguoiChoi;
            //set up huong sai
            _roundCount[GetCurrentTurn()]++;
            _lastWinPlayer = -2;
            Debug.Log("Round Histore: " + GetCurrentTurn());
        }

        public void SetFalseRound (int playerIndex)
		{
			_vaoVong [playerIndex] = false;
			//_soNguoiChoiInRound--;
		}

        //private int _countRoundFromWin = -1;
		public void NextTurn ()
		{
            _needReset = true;
            _huongSai = false;
            int c = _currentTurn;
            
            //kiem tra so nguoi choi con trong vong (khong tinh nhung nguoi da Toi)
            int nguoiChoiConLai = 0;
            int soNguoiChoiConLai = 0;
            for (int i = 0; i < _soNguoiChoi; i++)
            {
                Debug.Log(string.Format("Hit round : vao vong {0}:{1}", _vaoVong[i], _roundCount[i]));
                if (_vaoVong[i])
                {
                    nguoiChoiConLai = i;
                    soNguoiChoiConLai++;
                }
            }

            switch (soNguoiChoiConLai)
            {
                case 0:
                    _huongSai = true;
                    break;
                case 1:
                    if (_hadWin[nguoiChoiConLai])
                        _huongSai = true;
                    else
                        _currentTurn = nguoiChoiConLai;
                    break;                
                default:
                    for (int i = 0; i < _soNguoiChoi; i++)
                    {
                        c = (c + 1) % _soNguoiChoi;
                        //                    Debug.Log(string.Format("SequencePlayer : c {0}:{1}", sequencePlayer[i], c));
                        if (_vaoVong[c] && !_hadWin[c])
                        {
                            _needReset = false;
                            _currentTurn = c;
                            break;
                        }
                    }
                    break;
            }
            if (!_needReset)
                _roundCount[GetCurrentTurn()]++;
            //if (_countRoundFromWin >= 0) _countRoundFromWin++;
            Debug.Log("Round Histore: " + GetCurrentTurn());
            //set false player win sau khi tinh sau nguoi choi tiep theo  ***don't change this
            if (soNguoiChoiConLai <= 2)
            {
                Debug.Log("_lastWinPlayer == _currentTurn");
                for (int i = 0; i < _soNguoiChoi; i++)
                {
                    if (_hadWin[i])
                    {
                        _vaoVong[i] = false;
                    }
                }
            }
        }		

        //public void SetVaoVong(int index, bool b)
        //{
        //    _vaoVong[index] = b;
        //}

        public void SetCountPlayerInRound(int n)
        {
            //countInRound = n;
            _vaoVong[n] = true;
            _currentTurn = n;
            _roundCount[GetCurrentTurn()]++;
            Debug.Log("Round Histore: " + GetCurrentTurn());
        }

        public void ResetVaoVong()
        {
            for (var i = 0; i < _soNguoiChoi; i++)
                _vaoVong[i] = true;
        }

  //      public int GetPrevTurn(int luotHienTai) {
		//	int result = (luotHienTai + _soNguoiChoi-1) % _soNguoiChoi;
		//	return result;
		//}

		//public int GetNextTurn(int luotHienTai) {
		//	int result = (luotHienTai + 1) % _soNguoiChoi;
		//	return result;
		//}

		//public int GetPrevTurn() {
		//	int result = (_currentTurn + _soNguoiChoi-1) % _soNguoiChoi;
		//	return result;
		//}

		//public int GetNextTurn() {
		//	int result = (_currentTurn + 1) % _soNguoiChoi;
		//	return result;
		//}

        public bool GetHadWin(int index)
        {
            if (index < 0 || index >= _hadWin.Length)
                return true;
            return _hadWin[index];
        }

        public int GetCurrentTurn() {
            return _currentTurn;
        }

        public int GetSoNguoiChoiConDanh
        {
            get
            {
                var count = 0;
                foreach (var w in _hadWin)
                    if (!w)
                        count++;
                return count;
            }
        }

        public void SetHadWin(int playerIndex)
        {
            _hadWin[playerIndex] = true;
            _lastWinPlayer = playerIndex;
            //_countRoundFromWin = 0;
            //set up huong sai
        }

        public void SetCong(int playerIndex)
        {
            _hadWin[playerIndex] = true;
        }

        public bool NeedReset {
            get
            {
                return _needReset;
            }
        }
	}
}

