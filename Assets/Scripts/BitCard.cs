﻿using System.Collections.Generic;

public class BitsHandle
{
    public static ulong ListToUlong(List<int> list) //list count must < 52
    {
        ulong mark = Invalid;
        foreach (var item in list)
        {
            mark |= (1ul << item);
        }
        return mark;
    }

    public static List<int> UlongToList(ulong mark) //list count must < 52
    {
        List<int> result = new List<int>();
        ulong temp = Invalid;
        for (ulong i = 1; i < 64; i++)
        {
            temp = mark & (1ul << (int)i);
            if (temp == (1ul << (int)i)) { result.Add((int)i); }
        }
        return result;
    }

    public static ulong Invalid = 0;
}