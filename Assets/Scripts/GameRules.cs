﻿//ChangeLog
//Fix Player hit card in list cannot hit
//Fix Habainhonhat
//Fix Guibai
//bug: khong Ha bai phom an, khong ha phom >
// Add check tai luot
//Check U - thieu TH tai luot U -//Fixed
//U Tron - Add Tinh tien, check u tron.
//Fix: Tinh Xu An
//Bug - Tinh Xu An: Het tien van duoc cong.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TienLen
{
    public static class GameRules
    {
        /// <summary>
        /// So sanh 2 loai bai:
        /// - True khi: h2 == null || (h1 va h2 khac loai && Chat hàng) || (H1 va h2 cung loai && h1.maxCard > h2. maxCard)
        /// - False khi: truong hop con lai
        /// </summary>
        /// <param name="h1"></param>
        /// <param name="h2"></param>
        /// <returns></returns>
        public static bool SoSanh(HitCard h1, HitCard h2)
        {
            if (h2 == null)
                return true;
            if (h1.Type != h2.Type)
            {
                if (h2.Type == 1 && h2.MaxCard > 47 && (h1.Type == 4 || h1.Type >= 15)) // bat 2
                    return true;
                if (h2.Type == 2 && h2.MaxCard > 47 && (h1.Type == 4 || h1.Type >= 16)) // bat doi 2
                    return true;
                if (h2.Type == 15 && (h1.Type == 4 || h1.Type >= 16)) // bat 3 doi thong
                    return true;
                if (h2.Type == 4 && h1.Type >= 16) // bat tu quy
                    return true;
                if (h2.Type == 16 && h1.Type == 17) // bat 4 doi thong
                    return true;
                return false;
            }
            if (h1.MaxCard > h2.MaxCard)
                return true;
            return false;
        }


        public static int KiemTraBaiHopLe(List<int> temp)
        {
            int[] num = new int[13];
            temp.Sort();
            var length = temp.Count;
            // check single card
            if (length == 1)
                return 1; // single card
                          // access num of card
            for (int i = 0; i < length; i++)
            {
                num[i] = (int)temp[i];
            }
            // check doule card
            if (length == 2)
            {

                if (num[0] / 4 == num[1] / 4)
                    return 2; // double card
                else
                    return 0; // error
            }
            // check triple card
            if (length == 3)
            {
                if ((num[0] / 4 == num[1] / 4) && (num[1] / 4 == num[2] / 4))
                    return 3; //triple card
            }
            // check card four same
            if (length == 4)
            {
                if ((num[0] / 4 == num[1] / 4) && (num[1] / 4 == num[2] / 4) && (num[2] / 4 == num[3] / 4))
                    return 4; //four same card
            }
            // if 2 card has in list move forward this is error
            for (int i = 0; i < length; i++)
                if (num[i] > 47)
                    return 0;
            // check card move forward
            bool moveForward = true;
            for (int i = 1; i < length; i++)
                if (num[i] / 4 - num[i - 1] / 4 != 1)
                {
                    moveForward = false;
                    break;
                }
            if (moveForward)
                return length + 2;
            // check card double move forward
            if (length >= 6 && length % 2 == 0)
            {
                bool doubleMove = true;
                // check requirement for double move forward
                if (num[0] / 4 != num[1] / 4)
                    doubleMove = false;
                else
                    for (int i = 2; i < length; i++)
                        if (i % 2 == 0)
                            if (num[i] / 4 != num[i + 1] / 4 || num[i] / 4 - num[i - 1] / 4 != 1)
                            {
                                doubleMove = false;
                                break;
                            }
                // return true if have all requirement
                if (doubleMove)
                    return length / 2 + 12;
            }
            // all other return error
            return 0;
        }

        public static bool KiemTraHang(HitCard hit)
        {
            if (hit.MaxCard > 47
                || hit.Type == 4
                || hit.Type >= 15)
                return true;
            return false;
        }

        public static List<int> XepBaiTangDan(List<int> ds)
        {
            var danhSachBai = new List<int>(ds);
            //			var result = new List<int> ();
            danhSachBai.Sort();

            return danhSachBai;
        }

        private static int SoSanhHitCard(HitCard x, HitCard y)
        {
            if (x.MaxCard > 47 || y.MaxCard > 47)
            {
                if (x.MaxCard > y.MaxCard)
                    return 1;
                else
                    return -1;
            }
            else
            {
                if (x.ListCard.Count > y.ListCard.Count)
                    return 1;
                else if (x.ListCard.Count < y.ListCard.Count)
                    return -1;
                else
                    return 0;
            }
        }

        public static string GetName(HitCard hitCard)
        {
            if (hitCard == null) return " Hitcard == null";
            int MaxCard = hitCard.MaxCard;
            string result = "Kieu: ";

            switch (hitCard.Type)
            {
                case 1:
                    result += " Le - ";
                    break;
                case 2:
                    result += " Doi - ";
                    break;
                case 3:
                    result += " Khap - ";
                    break;
                case 4:
                    result += " Tu Quy - ";
                    break;
                case 5:
                    result += " Day 3 - ";
                    break;
                case 6:
                    result += " Day 4 - ";
                    break;
                case 7:
                    result += " Day 5 - ";
                    break;
                case 8:
                    result += " Day 6 - ";
                    break;
                case 9:
                    result += " Day 7 - ";
                    break;
                case 10:
                    result += " Day 8 - ";
                    break;
                case 11:
                    result += " Day 9 - ";
                    break;
                case 12:
                    result += " Day 10 - ";
                    break;
                case 13:
                    result += " Day 11 - ";
                    break;
                case 14:
                    result += " Day 12 - ";
                    break;
                case 15:
                    result += " 3 Doi Thong - ";
                    break;
                case 16:
                    result += " 4 Doi Thong - ";
                    break;
                case 17:
                    result += " 5 Doi Thong - ";
                    break;
            }

            string tmp = "";

            switch (MaxCard % 4)
            {
                case 0: tmp = "bich"; break;
                case 1: tmp = "chuong"; break;
                case 2: tmp = "ro"; break;
                case 3: tmp = "co"; break;
            }

            int tmpInt = (MaxCard / 4) + 3;

            switch (tmpInt)
            {
                case 11: result += 'J' + " " + tmp; break;
                case 12: result += 'Q' + " " + tmp; break;
                case 13: result += 'K' + " " + tmp; break;
                case 14: result += 'A' + " " + tmp; break;
                case 15: result += '2' + " " + tmp; break;
                default: result += tmpInt.ToString() + " " + tmp; break;
            }

            return result;
        }

        public static string GetName(int MaxCard)
        {
            string result = "";
            string tmp = "";

            switch (MaxCard % 4)
            {
                case 0: tmp = "bich"; break;
                case 1: tmp = "chuong"; break;
                case 2: tmp = "ro"; break;
                case 3: tmp = "co"; break;
            }

            int tmpInt = (MaxCard / 4) + 3;

            switch (tmpInt)
            {
                case 11: result += 'J' + " " + tmp; break;
                case 12: result += 'Q' + " " + tmp; break;
                case 13: result += 'K' + " " + tmp; break;
                case 14: result += 'A' + " " + tmp; break;
                case 15: result += '2' + " " + tmp; break;
                default: result += tmpInt.ToString() + " " + tmp; break;
            }

            return result;
        }
    }
}

