﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class ID
{
	public int index;
	public long money;
	public string name;

	public ID ()
	{
		index = 0;
		money = 0;
		name = "";
	}
}

public enum TypeLeader
{
	MyRank,
	Top
}
public class PrefsEventArgs: EventArgs
{
	public string keyPrefs;

	public PrefsEventArgs (string key)
	{
		keyPrefs = key;
	}
}
