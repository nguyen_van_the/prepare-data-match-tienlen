﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TienLen;
using UnityEngine;

public class ParseDuLieu_2_DataTrain : MonoBehaviour {
    bool parseAI = false;
    public void StartParseDataTrain()
    {
        if (!Directory.Exists(Application.dataPath + "/DataTrain"))
            Directory.CreateDirectory(Application.dataPath + "/DataTrain");
        //parseAI = parseDataAI == 1;
        if (!Directory.Exists(Application.dataPath + "/PhanTich"))
            Directory.CreateDirectory(Application.dataPath + "/PhanTich");
        //parseAI = parseDataAI == 1;
        LoadPhanTich();
        StartCoroutine(ProcessListMatch());
    }

    void LoadPhanTich()
    {
        var filePath = Application.dataPath + "/PhanTich/json.txt";
        if (File.Exists(filePath))
        {
            string jsonString = File.ReadAllText(filePath);
            phantichData = new JSONObject(jsonString);
            //Debug.Log(arrData.GetField("user").GetField("countPlay") + ". " + arrData.Print());
        }
    }

    public IEnumerator ProcessListMatch()
    {
        var folderFilePaths = Directory.GetFiles(Application.dataPath + "/MatchFilter").Where(name => !name.EndsWith(".meta") && !name.EndsWith(".DS_Store")).ToArray();

        //Debug.Log("file count: " + folderFilePaths.Length);
        List<JSONObject> result = new List<JSONObject>();
        foreach (var f in folderFilePaths)
        {
            string jsonString = File.ReadAllText(f);
            var jListMatch = new JSONObject(jsonString);
            jListMatch.AddField("jsonName", f);
            //Debug.Log("Tap tin name " + json.GetField("jsonName"));
            //Debug.Log("Number of json: " + jListMatch.GetField("jsonName") + " is " + jListMatch.Count);
            if (jListMatch.keys.Count <= 1)
            {
                Debug.Log("Tap tin sai dinh dang " + jListMatch.GetField("jsonName"));
                yield break;
            }
            var s = jListMatch.GetField("jsonName").str;
            var fileName = s.Substring(s.LastIndexOf('\\') + 1);
            foreach (var key in jListMatch.keys)
            {
                var jMatch = jListMatch[key];
                if (!jMatch.HasField("historyHit") || !jMatch.GetField("type").str.Equals("TruyenThong") || jMatch.GetField("aiTypes").list.Count != 4
                    || jMatch.GetField("bet").i <= 0 || jMatch.GetField("bxh").list[0].i != 1)
                {
                    //Debug.Log("Cac van An Trang, khong co history: " + jMatch);
                    continue;
                }
                else
                {
                    XuLy_NguoiThangNhieuXuNhat(jMatch);
                    PhanTich(jMatch);
                    //Debug.Log("Array Data: " + arrData.Count + " - " + key);
                }
                yield return null;// new WaitForSeconds(0.05f);
            }
            if (breakProcess) yield break;
            SaveText(phantichData.Print(), "json", Application.dataPath + "/PhanTich", new string[] {
                //typeKieuChoi.ToString() 
                }); 
#if !UNITY_EDITOR
            File.Delete(f);
#endif
        }        
    }
    Dictionary<string, JSONObject> arrData = new Dictionary<string, JSONObject>();
    JSONObject phantichData = new JSONObject();

    void PhanTich(JSONObject jMatch)
    {
        var nguoiThang = -1;
        var maxXu = long.MinValue;
        long bet = jMatch.GetField("bet").i;
        var moneyList = jMatch.GetField("moneyChange").list;
        for (var i = 0; i < moneyList.Count; i++)
        {
            if (maxXu < (long)moneyList[i].i)
            {
                maxXu = (long)moneyList[i].i;
                nguoiThang = i;
            }
        }
        JSONObject[] _listAi = jMatch.GetField("aiTypes").list.ToArray();
        JSONObject[] _listBxh = jMatch.GetField("bxh").list.ToArray();
        List<string> _countedAIList = new List<string>();

        for (var i = 0; i < _listAi.Length; i++)
        {
            var typePlayer = i == 0 ? "user" : _listAi[i].str;
            var typeKieuChoi = (KieuChoi)Enum.Parse(typeof(KieuChoi), jMatch.GetField("type").str);
            var key = typePlayer;

            if (!phantichData.HasField(key))
            {
                var json = new JSONObject();
                json.SetField("countPlay", 0);
                json.SetField("countPlayUnique", 0);
                json.SetField("countWin", 0);
                json.SetField("countXuDuong", 0);
                json.SetField("countXuAm", 0);
                phantichData.SetField(key, json);
            }
            var data = phantichData.GetField(key);
            if (!_countedAIList.Contains(key))
            {
                if (data.HasField("countPlayUnique"))
                    data.GetField("countPlayUnique").i++;
                else
                    data.SetField("countPlayUnique", 1);
                _countedAIList.Add(key);
            }
            data.GetField("countPlay").i++;
            if (_listBxh[i].i == 1 || _listBxh[i].i == 2)
                data.GetField("countWin").i++;
            if (moneyList[i].i > 0)
                data.GetField("countXuDuong").i += (int)moneyList[i].i / (int)bet;
            else
                data.GetField("countXuAm").i += (int)moneyList[i].i / (int)bet;
        }
    }
    void XuLy_NguoiThangNhieuXuNhat(JSONObject jMatch)
    {
        var nguoiThang = -1;
        var maxXu = long.MinValue;
        var moneyList = jMatch.GetField("moneyChange").list;
        for (var i = 0; i < moneyList.Count; i++)
        {
            if (maxXu < (long)moneyList[i].i)
            {
                maxXu = (long)moneyList[i].i;
                nguoiThang = i;
            }
        }
        //if (!parseAI && nguoiThang != 0) return;
        var thongSoVanBai = new ThongSoVanBai(4, jMatch.GetField("bet").i, 1, (KieuChoi)Enum.Parse(typeof(KieuChoi), jMatch.GetField("type").str));
        var enviroment = new Enviroment(thongSoVanBai, jMatch.GetField("firstPlayer").i, nguoiThang);
        //chia bai
        foreach (var c in jMatch.GetField("phatBai").list)
        {
            var listInt = c.GetField("list").list.Select((JSONObject j) => j.i).ToList();
            listInt.Sort();
            enviroment.ChiaBai(c.GetField("p").i, listInt);
        }

        var preRoundIndex = 0;
        var typePlayer = nguoiThang == 0? "Real" : "AI";
        var typeKieuChoi = thongSoVanBai.KieuChoi.ToString();        

        //bat dau quet history hitcard
        foreach (var c in jMatch.GetField("historyHit").list) {
            var playerHit = c.GetField("p").i;            
            var listInt = new List<int>();
            HitCard hit = null;
            var roundIndex = c.GetField("round").i;

            if (preRoundIndex != roundIndex)
                enviroment.ResetNewRound();

            if (c.GetField("list").list != null)
            {
                listInt = c.GetField("list").list.Select((JSONObject j) => j.i).ToList();
                listInt.Sort();
            }
            hit = new HitCard(playerHit, roundIndex, listInt);            
            preRoundIndex = roundIndex;
            var data = enviroment.NguoiChoiDanhBai(hit, playerHit == nguoiThang);
            if (playerHit == nguoiThang)
            {
                var key = typePlayer + '/' + typeKieuChoi;
                if (!arrData.ContainsKey(key))
                    arrData[key] = new JSONObject();
                arrData[key].Add(data);
                if (arrData[key].Count >= 1000) //luu 1000 set trong 1 file
                {
                    SaveText(arrData[key].Print(), DateTime.Now.ToFileTime().ToString(), Application.dataPath + "/DataTrain", new string[] {
                        typePlayer,
                        typeKieuChoi });
                    arrData[key] = new JSONObject();
                    Debug.Log(fileSaveCount++ + ". Saved");
                }
            }
        }
    }
    static int fileSaveCount = 0;
    public static void SaveText(string contents, string id, string SavedUrl, string[] subPaths)
    {
        if (!Directory.Exists(SavedUrl))
        {
            Directory.CreateDirectory(SavedUrl);
        }        
        foreach (var s in subPaths)
        {
            SavedUrl += "/" + s;
            if (!Directory.Exists(SavedUrl))
            {
                Directory.CreateDirectory(SavedUrl);
            }
        }
        string pathFile = SavedUrl + "/" + id + ".txt";
        //Debug.Log("pathFile: " + pathFile + " - id" + id);

        File.WriteAllText(pathFile, contents);
    }

    bool breakProcess = false;
    public void Break()
    {
        breakProcess = true;
    }
}
