﻿//Changelog
//Fix Xep Hang Hoi Bai
//Check: quan bai khong the danh - che do player - OK
//hoi bai - tai luot: ok
//Bug ha Bai: 7778910J - Ha 78910J - Fixed
//Xep Hang Tai Luot - Fixed
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using AILab;
//using TienLen.NgonNgu;

namespace TienLen
{
    public class Enviroment
    {
        protected VanBaiState _trangThaiVanBai = VanBaiState.None;
        protected EnviromentType _enviromentType = EnviromentType.TestAI;
        protected Dictionary<int, List<int>> _danhSachBaiBanDau = new Dictionary<int, List<int>>();
        protected readonly Dictionary<int, List<int>> _danhSachBaiHienTai = new Dictionary<int, List<int>>();
        protected Dictionary<int, List<int>> _danhSachBaiBiLoai = new Dictionary<int, List<int>>();
        protected Dictionary<int, int> _danhSachXepHang = new Dictionary<int, int>();
        protected List<int> _thuTuHaBai = new List<int>();
		protected List<HitCard> _danhSachBaiDaDanh = new List<HitCard> ();
        //protected TurnManager _turnManager;
        protected static int _nguoiChoiDauTien = -1;
        protected int _thuTuXepHang = 1;
        protected int _soHeoBiChat = 0;
        protected int _soHeoDenBiChat = 0;
        protected int _turnCount = 0;
        protected int _playerWin = 0;
        protected float _timeWaitToHit = 0;
        protected float delayAI = 0;
        //protected List<string> _aiTypes = new List<string> ();
		protected HitCard _lastCard;
        protected KetThucType _kieuKetThuc = KetThucType.Null;
        protected ThongSoVanBai _thongSoVanBai = new ThongSoVanBai();
        //protected List<ThongTinNguoiChoi> _thongTinNguoiChoi = new List<ThongTinNguoiChoi>();

        public Enviroment(ThongSoVanBai thongSo, int firstP, int pWin)
        {
            _thongSoVanBai = thongSo;
            _nguoiChoiDauTien = firstP;
            _playerWin = pWin;
            //Debug.Log("[_playerWin] " + pWin);
            Init();
        }

        //protected void BatDauVanBai()
        //{
        //    _trangThaiVanBai = VanBaiState.BatDau;            
        //}

        protected void Init()
        {
            for (int i = 0; i < _thongSoVanBai.SoNguoiChoi; i++)
            {
                _danhSachBaiBanDau[i] = new List<int>();
                _danhSachBaiHienTai[i] = new List<int>();
                _danhSachBaiBiLoai[i] = new List<int>();
                _danhSachXepHang[i] = 4; //4: xep hang bet; 5: cong; 0: an trang
            }
            //_turnManager = new TurnManager(_thongSoVanBai.SoNguoiChoi, _nguoiChoiDauTien);
			_lastCard = null;
            _thuTuXepHang = 1;
            _kieuKetThuc = KetThucType.Null;
        }

        public virtual void ChiaBai(int indexP, List<int> listBai)
        {
            for (var i = 0; i < listBai.Count; i++)
            {
                _danhSachBaiBanDau[indexP].Add(listBai[i]);
                _danhSachBaiHienTai[indexP].Add(listBai[i]);
            }
        }

        protected HitCard hitCard;
        protected int _roundIndex = 0;
        //public virtual IEnumerator DanhBai()
        //{
        //    BatDauVanBai();
        //    hitCard = null;

        //    while (true)
        //    {
        //        yield return new WaitForSeconds(_timeWaitToHit);
        //        var currentTurn = _turnManager.GetCurrentTurn();
        //        //Boc hoac An
        //        hitCard = _danhSachAI[currentTurn].SuyNghiDanh(_lastCard);
        //        if (hitCard == null)
        //        {
        //            NguoiChoiBoQua(currentTurn);
        //        }
        //        else {
        //            hitCard.SetPlayerHit(currentTurn);
        //            hitCard.SetRoundIndex(_roundIndex);
        //            //
        //            if (KiemTraBaiHopLe(hitCard))
        //            {
        //                NguoiChoiDanhBai(hitCard);
        //            } else
        //            {
        //                NguoiChoiBoQua(currentTurn);
        //            }
        //            //KiemTraVaDanhBai(hitCard);
        //        }

        //        foreach (var ai in _danhSachAI)
        //            ai.PlayerDanhBai(hitCard);
        //        if (_thuTuXepHang > 1)
        //        {                    
        //            if (GameRules.CheckKetThuc(_thongSoVanBai.KieuChoi, new Dictionary<int, int>(_danhSachXepHang)))
        //            {                      
        //                if (_thongSoVanBai.KieuChoi == KieuChoi.TruyenThong && _thongSoVanBai.SoNguoiChoi > _thuTuXepHang)
        //                {
        //                    for (var i = 0; i < _thongSoVanBai.SoNguoiChoi; i++) 
        //                        if (_danhSachXepHang[i] == 4)
        //                        {
        //                            _danhSachXepHang[i] = _thuTuXepHang++;
        //                        }
        //                }
        //                _kieuKetThuc = KetThucType.Thuong;
        //                KiemTraChatHang();
        //                KetThucVanBai();
        //                break;
        //            }
        //        }
        //        _turnManager.NextTurn();
        //        if (_turnManager.NeedReset || _turnManager.GetCurrentTurn() == _lastCard.PlayerHit)
        //        {
        //            _turnManager.ResetNewRound();
        //            _lastCard = null;
        //            KiemTraChatHang();
        //            _roundIndex++;
        //        }
        //        hitCard = null;
        //        _turnCount++;
        //    }
        //}


        protected virtual void NguoiChoiBoQua(int currentTurn)
        {
            _lastCard = null;
            //Debug.Log(_roundIndex + ". Nguoi choi: " + currentTurn + " bo luot.");
        }

        public virtual void ResetNewRound()
        {

            //_turnManager.ResetNewRound();
            _lastCard = null;
            _roundIndex++;
            //Debug.Log("[ResetNewRound]");
        }

        protected virtual bool KiemTraBaiHopLe(HitCard hitCard)
        {
            if (GameRules.KiemTraBaiHopLe(hitCard.ListCard) != 0)
            {
                //xu ly danh
                if (GameRules.SoSanh(hitCard, _lastCard))
                {
                    //danh bai hitCard
                    //NguoiChoiDanhBai(hitCard.PlayerHit, hitCard);
                    return true;
                }
                else {
                    //gui su kien bai be hon _lastHit
                    //NotiBoard.FadeText(NgonNguFactory.Instance.BiggerCard);                    
                }
            }
            else {
                //gui su kien bai bi loi
                //NotiBoard.FadeText(NgonNguFactory.Instance.InvalidCard);
            }
            return false;
        }

        public virtual JSONObject NguoiChoiDanhBai(HitCard hitCard, bool getString = false)
        {
            var jsonData = new JSONObject();
            var jsonValues = new JSONObject();
            var dsHitCard = new List<HitCard>();
            //get Date
            if (getString) //chi khi duoc yeu cau moi tra ve ket qua train
            {
                jsonValues = JsonDataTrain();                
                dsHitCard.Add(new HitCard(_playerWin, _roundIndex, new List<int>()));
                //if (_lastCard != null)
                //    Debug.Log("_lastCard" + GameRules.GetName(_lastCard));
                dsHitCard.AddRange(QuetBai.Scan(BitsHandle.ListToUlong(_danhSachBaiHienTai[_playerWin]), _lastCard));
                //jsonData.Add(dsHitCard.Count); //tong so lua chon
            }
            jsonData.AddField("Values", jsonValues);

            //Get Targets
            var jsonTargets = new JSONObject();
            if (hitCard == null || hitCard.ListCard.Count == 0)
            {
                if (getString) //chi khi duoc yeu cau moi tra ve ket qua train
                    AddListToDataTrain(jsonTargets, null, 52);
                NguoiChoiBoQua(hitCard.PlayerHit);
            }
            else
            {
                //Debug.Log(_roundIndex + ". Nguoi choi: " + hitCard.PlayerHit + " danh la bai: " + GameRules.GetName(hitCard));
                if (getString) //chi khi duoc yeu cau moi tra ve ket qua train
                {
                    AddListToDataTrain(jsonTargets, hitCard.ListCard, 52);

                    //var flagExist = false;
                    //for (var i = 0; i < dsHitCard.Count; i++)
                    //{
                    //    if (hitCard.ListCard_Long == dsHitCard[i].ListCard_Long)
                    //    {
                    //        flagExist = true;
                    //        break;
                    //    }
                    //}
                    //if (!flagExist) Debug.LogError("Scan all khong ton tai HitCard: " + GameRules.GetName(hitCard));
                }
                foreach (var c in hitCard.ListCard)
                {
                    _danhSachBaiHienTai[hitCard.PlayerHit].Remove(c);
                    _danhSachBaiBiLoai[hitCard.PlayerHit].Add(c);
                }
                _danhSachBaiDaDanh.Add(hitCard);
                _lastCard = hitCard;
            }
            jsonData.AddField("Targets", jsonTargets);
            //Debug.Log("_lastCard" + GameRules.GetName(_lastCard));
            return jsonData;
        }

        public JSONObject JsonDataTrain()
        {
            JSONObject json = new JSONObject();
            //get N1, N2, N3
            for (var i = 0; i < 4; i++)
                if (i != _playerWin){
                    AddNumberTrain(json, _danhSachBaiHienTai[i].Count, 13);              
                }
            //get M
            if (_lastCard != null) //last card;
                AddListToDataTrain(json, new List<int>(_lastCard.PlayerHit), 4);
            else
                AddListToDataTrain(json, null, 4); //bat dau van bai, chua co ai danh
            //get ListBai
            var listBaiDaDanh = new List<int>();
            foreach (var h in _danhSachBaiDaDanh)
                foreach (var c in h.ListCard)
                    listBaiDaDanh.Add(c);
            AddListToDataTrain(json, listBaiDaDanh, 52); //list card p4;
            if (_lastCard != null) //last card;
                AddListToDataTrain(json, _lastCard.ListCard, 52); //list card p4;
            else
                AddListToDataTrain(json, null, 52);
            AddListToDataTrain(json, _danhSachBaiHienTai[_playerWin], 52); //list card p4;
            return json;
        }

        void AddNumberTrain(JSONObject json, int listCount, int count)
        {
            var arr = new JSONObject();
            for (var i = 0; i < count; i++)
                if (i < listCount)
                    arr.Add(1f);
                else
                    arr.Add(0f);
            foreach (var o in arr.list)
                json.Add(o);
            //Debug.Log("test list to bit data: " + arr.ToString());
            //return arr;
        }

        void AddListToDataTrain(JSONObject json, List<int> list, int count) {
            var arr = new JSONObject();
            for (var i = 0; i < count; i++)
                arr.Add(0f);
            if (list != null && list.Count != 0)
            {
                string s = "";
                foreach (var x in list)
                    if (x < count)
                    {
                        s += x + " ";
                        arr[x].n = 1.0f;
                    }
                //Debug.Log(s);

            }
            foreach (var o in arr.list)
                json.Add(o);
            //Debug.Log("test list to bit data: " + arr.ToString());
            //return arr;
        }

        protected virtual void KetThucVanBai()
        {
        }
    }

    public enum VanBaiState
    {
        None,
        BatDau,
        ChonLuaBocAn,
        SuyNghiDanh,
        DangDanh,
        KetThuc
    }

    public enum KieuChoi
    {
        TruyenThong,
        DemLa,        
        NhatAnTat
    }

    public enum KetThucType
    {
        Thuong,
        AnTrang,
        Null
    }

    public enum EnviromentType
    {
        TestAI,
        Play
    }

    public enum ActionHit
    {
        Null,
        BaBich,
		BonThong,
        Danh,
        ChoQua,
		Wait
    }
}
