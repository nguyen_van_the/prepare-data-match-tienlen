﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TienLen;
using UnityEngine;

public class PhanTichDuLieu : MonoBehaviour {
    bool parseAI = false;
    JSONObject arrData = new JSONObject();  
    List<string> user = new List<string>();
    List<string> dollar = new List<string>();
    List<string> dong = new List<string>();
    List<string> franc = new List<string>();
    int numOfbestGame_user = 0;
    int numOfbestGame_dollar = 0;
    int numOfbestGame_dong = 0;
    int numOfbestGame_franc = 0;
    int countMatch = 0;
    public void StartPhanTIch()
    {
        if (!Directory.Exists(Application.dataPath + "/PhanTich"))
            Directory.CreateDirectory(Application.dataPath + "/PhanTich");
        //parseAI = parseDataAI == 1;
        //LoadPhanTich();
        StartCoroutine(ProcessListMatch());
        
    }

    void LoadPhanTich() {
        var filePath = Application.dataPath + "/PhanTich/json.txt";
        if (File.Exists(filePath))
        {
            string jsonString = File.ReadAllText(filePath);
            
            //Debug.Log(arrData.GetField("user").GetField("countPlay") + ". " + arrData.Print());
        }
    }

    public IEnumerator ProcessListMatch()
    {
        var folderFilePaths = Directory.GetFiles(Application.dataPath + "/MatchFilter").Where(name => !name.EndsWith(".meta") && !name.EndsWith(".DS_Store")).ToArray();
        

       
        
        Debug.LogError("file count: " + folderFilePaths.Length);
        List<JSONObject> result = new List<JSONObject>();
        int count = 0;
        foreach (var f in folderFilePaths)
        {           
            string jsonString = File.ReadAllText(f);
            var jListMatch = new JSONObject(jsonString);
            jListMatch.AddField("jsonName", f);
            //Debug.Log("Tap tin name " + json.GetField("jsonName"));
            //Debug.Log("Number of json: " + jListMatch.GetField("jsonName") + " is " + jListMatch.Count);
            if (jListMatch.keys.Count <= 1)
            {
                Debug.Log("Tap tin sai dinh dang " + jListMatch.GetField("jsonName"));
                yield break;
            }
            var s = jListMatch.GetField("jsonName").str;
            var fileName = s.Substring(s.LastIndexOf('\\') + 1);
            foreach (var key in jListMatch.keys)
            {
                var jMatch = jListMatch[key];
                if (!jMatch.HasField("historyHit") || !jMatch.GetField("type").str.Equals("TruyenThong") || jMatch.GetField("aiTypes").list.Count != 4
                   || jMatch.GetField("bet").i <= 0)
                {
                    //Debug.Log("Cac van An Trang, khong co history: " + jMatch);
                    continue;
                }
                else
                {
                    Debug.LogError("id :" + jMatch.GetField("_id").ToString());
                    PhanTich(jMatch);
                    
                    //Debug.Log("Array Data: " + arrData.Count + " - " + key);
                }
                yield return null;// new WaitForSeconds(0.05f);
            }
            
            if (breakProcess) yield break;

           
            Debug.Log("count :" + count++);
#if !UNITY_EDITOR
            File.Delete(f);
#endif
            
        }
       
        Debug.Log("numOfbestGame_user :" + numOfbestGame_user);
        Debug.Log("numOfbestGame_dollar :" + numOfbestGame_dollar);
        Debug.Log("numOfbestGame_dong :" + numOfbestGame_dong);
        Debug.Log("numOfbestGame_franc :" + numOfbestGame_franc);
        WriteXuTxt(user, dollar, dong, franc);
        SaveText(arrData.Print(), "json1", Application.dataPath + "/PhanTich", new string[] {
                //typeKieuChoi.ToString() 
        });


    }
    //Dictionary<string, JSONObject> arrData = new Dictionary<string, JSONObject>();
    void WriteXuTxt(List<string> user, List<string> dollar, List<string> dong, List<string> franc)
    {
        var filePath = Application.dataPath + "/PhanTich/json.txt";
        DirectoryInfo[] cDirs = new DirectoryInfo(filePath).GetDirectories();
        var i = 0;
        List<string> text = new List<string>();
        int length = dollar.Count;
        if (franc.Count > dong.Count)
        {
            if (length > dong.Count) length = dong.Count;
        }
        else
        {
            if (length > franc.Count) length = franc.Count;
        }
        Debug.Log("length :" + length);
        while(i < length)
        {
            string record = i.ToString() + " "+ user[i] + " " + dollar[i] + " " + dong[i] + " " + franc[i];
            text.Add(record);
            i++;
        }
       

        using (StreamWriter sw = new StreamWriter(filePath))
        {
            foreach(var s in text)
             sw.WriteLine(s);         
        }
    }
    void PhanTich(JSONObject jMatch)
    {
        //if (!arrData.HasField("countMatch"))
        //    arrData.SetField("countMatch", 0);
        //arrData.GetField("countMatch").i++;

        var nguoiThang = -1; 
        var maxXu = long.MinValue;
        int Xu;
       
        long bet = jMatch.GetField("bet").i;
        var moneyList = jMatch.GetField("moneyChange").list;
        for (var i = 0; i < moneyList.Count; i++)
        {
            if (maxXu < (long)moneyList[i].i)
            {
                maxXu = (long)moneyList[i].i;
                nguoiThang = i;
            }
        }
        JSONObject[] _listAi = jMatch.GetField("aiTypes").list.ToArray();
        JSONObject[] _listBxh = jMatch.GetField("bxh").list.ToArray();
        // List<string> _countedAIList = new List<string>();
        string winner = "0";
        switch (nguoiThang)
        {
            case 0:
                winner = "user";
                break;
            case 1:
                winner = _listAi[1].str;
                break;
            case 2:
                winner = _listAi[2].str;
                break;
            case 3:
                winner = _listAi[3].str;
                break;
        }
        if(winner == "user")
        {
            numOfbestGame_user++;
        }
        if (winner == "dollar")
        {
            numOfbestGame_dollar++;
        }
        if (winner == "dong")
        {
            numOfbestGame_dong++;
        }
        if (winner == "franc")
        {
            numOfbestGame_franc++;
        }
        for (var i = 0; i < _listAi.Length; i++)
        {
            var typePlayer = i == 0 ? "user" : _listAi[i].str;
            var typeKieuChoi = (KieuChoi)Enum.Parse(typeof(KieuChoi), jMatch.GetField("type").str);
            var key = typePlayer;
            
            
            if (!arrData.HasField(key))
            {
                var json = new JSONObject();

                json.SetField("Xu", 0);           
                arrData.SetField(key, json);
            }

            var data = arrData.GetField(key);
            if (moneyList[i].i > 0)
                data.GetField("Xu").i += (int)moneyList[i].i / (int)bet;
            else
                data.GetField("Xu").i += (int)moneyList[i].i / (int)bet;
            Xu = data.GetField("Xu").i;

            //Debug.Log("Xu : " + Xu);                      
            if (key == "user")
            {              
                user.Add(Xu.ToString());
                //Debug.Log("xu user van 1 :" + user[0]);
            }
            if (key == "dollar")
            {                             
                dollar.Add(Xu.ToString());
                //Debug.Log("xu dollar van 1 :" + dollar[0]);
            }
            if (key == "dong")
            {               
                dong.Add(Xu.ToString());
               // Debug.Log("xu dong van 1 :" + dong[0]);
            }
            if (key == "franc")
            {            
                franc.Add(Xu.ToString());
               // Debug.Log("xu franc van 1 :" + franc[0]);
            }
        }
    }
    static int fileSaveCount = 0;
    public static void SaveText(string contents, string id, string SavedUrl, string[] subPaths)
    {
        if (!Directory.Exists(SavedUrl))
        {
            Directory.CreateDirectory(SavedUrl);
        }        
        foreach (var s in subPaths)
        {
            SavedUrl += "/" + s;
            if (!Directory.Exists(SavedUrl))
            {
                Directory.CreateDirectory(SavedUrl);
            }
        }
        string pathFile = SavedUrl + "/" + id + ".txt";
        //Debug.Log("pathFile: " + pathFile + " - id" + id);
        Debug.Log(fileSaveCount++ + ". Saved");
        File.WriteAllText(pathFile, contents);
    }

    bool breakProcess = false;
    public void Break()
    {
        breakProcess = true;
    }
}
