﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TienLen
{
    public class QuetBai
    {
        // Scan all of type card can hit of players
        public static List<HitCard> Scan(ulong listCard)
        {
            if (listCard == 0)
                return null;

            var danhSachHitCard = new List<HitCard>();
            danhSachHitCard.AddRange(QuetDoiThong(5, listCard));
            danhSachHitCard.AddRange(QuetDoiThong(4, listCard));            
            danhSachHitCard.AddRange(QuetDoiThong(3, listCard));
            danhSachHitCard.AddRange(QuetAllKhap(4, listCard));
            danhSachHitCard.AddRange(QuetAllKhap(3, listCard));
            danhSachHitCard.AddRange(QuetAllKhap(2, listCard));
            danhSachHitCard.AddRange(QuetAllKhap(1, listCard));
            for (var i = 12; i >= 3; i--)
                danhSachHitCard.AddRange(QuetAllDay(i, listCard));
            //foreach (var h in danhSachHitCard)
            //    Debug.Log("Scan: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        /// <summary>
        /// Ham scan Danh Sach Bai de phat hien xem trong danh sach co' bai de danh tra 1 loai bai nao do k
        /// </summary>
        /// <param name="danhSachQuanBai"></param>
        /// <param name="lastCard"></param>
        /// <returns></returns>
        public static List<HitCard> Scan(ulong listCard, HitCard lastCard = null)
        {
            if (listCard == 0)
                return null;
            if (lastCard == null)
                return ScanAll(listCard);

            var danhSachHitCard = new List<HitCard>();
            if (lastCard.Type <= 4)
            {
                danhSachHitCard.AddRange(QuetAllKhap(lastCard.Type, listCard));
            }
            else if (lastCard.Type <= 14)
            {
                danhSachHitCard.AddRange(QuetAllDay(lastCard.Type - 2, listCard));
            }
            else if (lastCard.Type <= 18)
            {
                danhSachHitCard.AddRange(QuetDoiThong(lastCard.Type - 12, listCard));
            }
            if (lastCard.Type == 1 && lastCard.MaxCard > 47)
            {
                danhSachHitCard.AddRange(QuetAllKhap(4, listCard));
                danhSachHitCard.AddRange(QuetDoiThong(3, listCard));
                danhSachHitCard.AddRange(QuetDoiThong(4, listCard));
                danhSachHitCard.AddRange(QuetDoiThong(5, listCard));
            }
            if ((lastCard.Type == 2 && lastCard.MaxCard > 47) || lastCard.Type == 15)
            {
                danhSachHitCard.AddRange(QuetAllKhap(4, listCard));
                danhSachHitCard.AddRange(QuetDoiThong(4, listCard));
                danhSachHitCard.AddRange(QuetDoiThong(5, listCard));
            }
            //foreach (var h in danhSachHitCard)
            //    Debug.Log("Scan: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        /// <summary>
        /// Quet tat ca cac loai bai co the danh duoc
        /// </summary>
        /// <param name="danhSachQuanBai"></param>
        /// <returns></returns>
        public static List<HitCard> ScanAll(ulong listCard)
        {
            if (listCard == 0)
                return null;

            var danhSachHitCard = new List<HitCard>();
            danhSachHitCard.AddRange(QuetDoiThong(5, listCard));
            danhSachHitCard.AddRange(QuetDoiThong(4, listCard));
            danhSachHitCard.AddRange(QuetDoiThong(3, listCard));
            for (var i = 12; i >= 3; i--)
            {
                danhSachHitCard.AddRange(QuetAllDay(i, listCard));
            }
            for (var i = 4; i >= 0; i--)
            {
                danhSachHitCard.AddRange(QuetAllKhap(i, listCard));
            }
            //foreach (var h in danhSachHitCard)
            //    Debug.Log("ScanAll: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        //public static bool KiemTraHang(HitCard hitCard)
        //{
        //    if ((hitCard.MaxCard > 47 && hitCard.Type < 3) || hitCard.Type == 4 || hitCard.Type > 14)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public static List<HitCard> QuetHeo(ulong listCard)
        //{
        //    var danhSachQuanBai = BitsHandle.UlongToList(listCard);
        //    var danhSachHitCard = new List<HitCard>();

        //    var tempList = danhSachQuanBai.Where(t => t > 47).ToList();
        //    if (tempList.Count > 0)
        //    {
        //        foreach (var t in tempList)
        //            danhSachQuanBai.Remove(t);
        //        danhSachHitCard.Add(new HitCard(tempList.Count, -1, tempList));
        //    }
        //    //RemoveDanhSach(listCard, danhSachHitCard);
        //    return danhSachHitCard;
        //}

        // check four same card
        //public static IEnumerable<HitCard> QuetKhap(int loaiKhap, ulong listCard)
        //{
        //    var danhSachQuanBai = BitsHandle.UlongToList(listCard);
        //    var danhSachHitCard = new List<HitCard>();
        //    for (var i = 0; i < danhSachQuanBai.Count - loaiKhap + 1; i++)
        //    {
        //        var isKhap = true;
        //        for (var j = i + 1; j < i + loaiKhap; j++)
        //            if (danhSachQuanBai[i] / 4 != danhSachQuanBai[j] / 4)
        //            {
        //                isKhap = false;
        //                break;
        //            }
        //        if (!isKhap)
        //            continue;
        //        {
        //            var tempList = new List<int>();
        //            for (var j = i; j < i + loaiKhap; j++)
        //                tempList.Add(danhSachQuanBai[j]);
        //            danhSachHitCard.Add(new HitCard(loaiKhap, -1, tempList));
        //            i += tempList.Count - 1;
        //        }
        //    }
        //    //RemoveDanhSach(listCard, danhSachHitCard);
        //    return danhSachHitCard;
        //}

        // check four same card
        public static IEnumerable<HitCard> QuetAllKhap(int loaiKhap, ulong listCard)
        {
            var danhSachQuanBai = BitsHandle.UlongToList(listCard);
            var danhSachHitCard = new List<HitCard>();
            for (var i = 0; i < danhSachQuanBai.Count - loaiKhap + 1; i++)
            {
                var isKhap = true;
                for (var j = i + 1; j < i + loaiKhap; j++)
                    if (danhSachQuanBai[i] / 4 != danhSachQuanBai[j] / 4)
                    {
                        isKhap = false;
                        break;
                    }
                if (!isKhap)
                    continue;
                {
                    var tempList = new List<int>();
                    for (var j = i; j < i + loaiKhap; j++)
                        tempList.Add(danhSachQuanBai[j]);
                    danhSachHitCard.Add(new HitCard(loaiKhap, -1, tempList));
                }
            }
            //RemoveDanhSach(listCard, danhSachHitCard);
            //foreach (var h in danhSachHitCard)
            //    Debug.Log(loaiKhap + ". QuetAllKhap: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        // check card double forward
        public static IEnumerable<HitCard> QuetDoiThong(int loaiDoiThong, ulong listCard)
        {
            var danhSachQuanBai = BitsHandle.UlongToList(listCard);
            var danhSachHitCard = new List<HitCard>();            
            if (danhSachQuanBai.Count < loaiDoiThong * 2)
                return danhSachHitCard;

            for (int i = 0; i < danhSachQuanBai.Count - loaiDoiThong * 2 + 1; i++)
                if (danhSachQuanBai[i] <= 47)
                {
                    var doiThong = false;
                    var coDoi = false;
                    var count = 1;
                    var tempList = new List<int> { danhSachQuanBai[i] };
                    for (var j = i + 1; j < danhSachQuanBai.Count; j++)
                        if (danhSachQuanBai[j] <= 47)
                        {
                            var delta = danhSachQuanBai[j] / 4 - danhSachQuanBai[j - 1] / 4;
                            if (delta == 0)
                            {
                                if (!coDoi)
                                    tempList.Add(danhSachQuanBai[j]);
                                coDoi = true;
                                if (count == loaiDoiThong)
                                {
                                    doiThong = true;
                                    break;
                                }
                            }
                            else if (delta == 1 && coDoi)
                            {
                                count++;
                                tempList.Add(danhSachQuanBai[j]);
                                coDoi = false;
                            }
                            else
                            {
                                break;
                            }
                        }
                    if (doiThong)
                    {
                        danhSachHitCard.Add(new HitCard(12 + loaiDoiThong, -1, tempList));
                        //i += tempList.Count - 1;
                    }
                }
            //RemoveDanhSach(listCard, danhSachHitCard);
            //foreach (var h in danhSachHitCard)
            //    Debug.Log("QuetAllDoiThong: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        // check card double forwardte
        //public static IEnumerable<HitCard> QuetDay(int loaiDay, ulong listCard)
        //{
        //    var danhSachQuanBai = BitsHandle.UlongToList(listCard);
        //    var danhSachHitCard = new List<HitCard>();

        //    for (var i = 0; i < danhSachQuanBai.Count - 2; i++)
        //        if (danhSachQuanBai[i] < 48)
        //        {
        //            var count = 1;
        //            var tempList = new List<int> { danhSachQuanBai[i] };
        //            for (var j = i + 1; j < danhSachQuanBai.Count; j++)
        //                if (danhSachQuanBai[j] < 48)
        //                {
        //                    var delta = danhSachQuanBai[j] / 4 - danhSachQuanBai[j - 1] / 4;
        //                    if (delta == 0)
        //                    {
        //                        if (count == loaiDay)
        //                        {
        //                            tempList.RemoveAt(tempList.Count - 1);
        //                            tempList.Add(danhSachQuanBai[j]);
        //                            danhSachHitCard.RemoveAt(danhSachHitCard.Count - 1);
        //                            danhSachHitCard.Add(new HitCard(2 + count,
        //                                -1, tempList));
        //                        }
        //                    }
        //                    else if (delta == 1)
        //                    {
        //                        count++;
        //                        tempList.Add(danhSachQuanBai[j]);
        //                        if (count == loaiDay)
        //                        {
        //                            danhSachHitCard.Add(new HitCard(2 + count, -1, tempList));
        //                            break;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        break;
        //                    }
        //                }
        //            if (count != loaiDay)
        //                continue;
        //            i += count - 1;
        //        }
        //    //RemoveDanhSach(listCard, danhSachHitCard);
        //    return danhSachHitCard;
        //}

        public static IEnumerable<HitCard> QuetAllDay(int loaiDay, ulong listCard)
        {
            var danhSachQuanBai = BitsHandle.UlongToList(listCard);
            var danhSachHitCard = new List<HitCard>();

            for (var i = 0; i < danhSachQuanBai.Count - 2; i++)
                if (danhSachQuanBai[i] < 48)
                {
                    var count = 1;
                    var tempList = new List<int> { danhSachQuanBai[i] };
                    for (var j = i + 1; j < danhSachQuanBai.Count; j++)
                        if (danhSachQuanBai[j] < 48)
                        {
                            var delta = danhSachQuanBai[j] / 4 - danhSachQuanBai[j - 1] / 4;
                            if (delta == 0)
                            {
                                if (count == loaiDay)
                                {
                                    tempList.RemoveAt(tempList.Count - 1);
                                    tempList.Add(danhSachQuanBai[j]);
                                    danhSachHitCard.Add(new HitCard(2 + count,
                                        -1, tempList));
                                }
                            }
                            else if (delta == 1)
                            {
                                count++;
                                tempList.Add(danhSachQuanBai[j]);
                                if (count == loaiDay)
                                    danhSachHitCard.Add(new HitCard(2 + count, -1, tempList));
                            }
                            else
                            {
                                break;
                            }
                        }
                }
            //RemoveDanhSach(listCard, danhSachHitCard);
            //foreach (var h in danhSachHitCard)
            //    Debug.Log("QuetAllDay: " + GameRules.GetName(h));
            return danhSachHitCard;
        }

        //private static void RemoveDanhSach(ulong listCard, List<HitCard> danhSachHitCard)
        //{
        //    var danhSachQuanBai = BitsHandle.UlongToList(listCard);
        //    foreach (var h in danhSachHitCard)
        //    {
        //        foreach (var t in h.ListCard)
        //        {
        //            danhSachQuanBai.Remove(t);
        //        }
        //    }
        //}

        private static bool ContainsAll<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            foreach (var e in b)
                if (!a.Contains(e))
                    return false;
            return true;
        }
    }
}