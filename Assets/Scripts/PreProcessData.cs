﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
/// <summary>
/// Class xu ly du lieu bi trung va loai cac truong hop an trang ra
/// </summary>
public class PreProcessData : MonoBehaviour
{
    static MD5 md5Hash;
    Dictionary<string, string> ListMatchMd5 = new Dictionary<string, string>();
    // Use this for initialization
    void Start()
    {
        Application.runInBackground = true;
        md5Hash = MD5.Create();        
    }

    public void StartPrepareData()
    {
        StartCoroutine(ProcessListMatch());
    }

    void ProcessJsonObj(JSONObject jListMatch)
    {
        Debug.Log("Number of json: " + jListMatch.GetField("jsonName") + " is " + jListMatch.Count);        
        if (jListMatch.keys.Count <= 1)
        {
            Debug.Log("Tap tin sai dinh dang " + jListMatch.GetField("jsonName"));
            return;
        }
        var s = jListMatch.GetField("jsonName").str;
        var fileName = s.Substring(s.LastIndexOf('\\') + 1);
        var jsonNew = new JSONObject();
        foreach (var key in jListMatch.keys)
        {            
            var jMatch = jListMatch[key];            
            if (!jMatch.HasField("historyHit"))
            {
                Debug.Log("Cac van An Trang, khong co history: " + jMatch);
                continue;
            }
            var md5 = GetMd5Hash(jMatch.GetField("historyHit").ToString());
            var contain = ListMatchMd5.Values.Contains(md5);
            var keyDic = fileName + "-" + key;
            if (!contain) //neu khong trung thi luu vao list
                ListMatchMd5[keyDic] = md5;
            else
            {
                var keyTrung = ListMatchMd5.First(x => x.Value == md5).Key;
                Debug.Log(keyDic + "trùng md5 vơi " + keyTrung + " Code MD5: " + md5);
                continue;
            }
            jsonNew.Add(jMatch);
        }
        SaveText(jsonNew.ToString(), fileName, Application.dataPath + "/MatchFilter");
    }

    public static void SaveText(string contents, string id, string SavedUrl)
    {
        if (!Directory.Exists(SavedUrl))
        {
            Directory.CreateDirectory(SavedUrl);
        }

        string pathFile = SavedUrl + "/" + id + ".txt";

        File.WriteAllText(pathFile, contents);
    }

    public static string LoadText(string id, string SavedUrl)
    {
        string pathFile = SavedUrl + "/" + id;

        if (File.Exists(pathFile))
        {
            return File.ReadAllText(pathFile);
        }
        else
            return "";
    }

    public IEnumerator ProcessListMatch()
    {
        var folderFilePaths = Directory.GetFiles(Application.dataPath + "/Matchs").Where(name => !name.EndsWith(".meta")).ToArray();

        Debug.Log("file count: " + folderFilePaths.Length);
        List<JSONObject> result = new List<JSONObject>();
        foreach (var f in folderFilePaths)
        {
            string jsonString = File.ReadAllText(f);
            var json = new JSONObject(jsonString);
            json.AddField("jsonName", f);
            ProcessJsonObj(json);            
            yield return new WaitForSeconds(1f);
            if (breakProcess) yield break;
        }
    }

    bool breakProcess = false;
    public void Break()
    {
        breakProcess = true;
    }

    #region MD5
    static string GetMd5Hash(string input)
    {

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

    // Verify a hash against a string.
    static bool VerifyMd5Hash(string input, string hash)
    {
        // Hash the input.
        string hashOfInput = GetMd5Hash(input);

        // Create a StringComparer an compare the hashes.
        StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        if (0 == comparer.Compare(hashOfInput, hash))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion
}
